#Informaçoes do App

Esqueleto de uma aplicação usando Slim framework

##Estrutura
* O conteúdo HTML do app está na pasta views/ com extenção .php, cada arquivo chama os partials _header.php e _footer.php que montam o cabeçalho e rodapé de cada página 
* Arquivos estáticos estão dentro da pasta public/assets/ lá se encontra css/, js/, img/, fonts/, e tudo mais que é carregado estaticamente
* Toda parte lógica, roteamento e persistencia está dentro pra pasta controllers/ onde cada .php é responsável pelo roteamento e lógica de cada parte do app
* Configurações do banco de dados e classe de abstração estão dentro do diretorio config/

##Requisitos do servidor
* PHP >= 5.3
* Apache com ModRewrite ativado
* Mysql