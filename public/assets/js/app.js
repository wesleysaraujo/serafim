//Base URL Global
base_url = $('base').context.URL;

var app = {
	init: function()
	{
		$(document.body).on('click', '#btn-exemp-event', function(){
			$.ajax({
				url: base_url+'/hello_json',
				type: 'GET',
				dataType: 'json',
				success: function(response)
				{
     				if(response.status === 200)
     				{
     					$('#response').append("<p class='text-success'>"+response.message+" <a href='javascript:{}' id='limpar'>Limpar</a></p>");
     					
     					$(document.body).on('click', '#limpar', function(event) {
     						$(this).parent().remove();
     					});
     				}
     				else
     				{
     				 	alert(":(");
     				}
				},
				error: function(response)
				{
					console.log(response);
				}
			});
		});
	}, 
}

jQuery(document).ready(function($) {
	//Instance the object init
	app.init();
});