	<br>
	<div class="jumbotron">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					&copy; <?php echo date('Y'); ?>
					<div class="pull-right">
						<h4>Wesley S. Araújo</h4>
					</div>	
				</div>
			</div>
		</div>
	</div>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="//code.jquery.com/jquery.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="<?php echo $base_url ?>/public/assets/js/bootstrap.min.js"></script>
      <script src="<?php echo $base_url ?>/public/assets/js/app.js"></script>
      <base href="<?php echo $base_url ?>">
    </body>
</html>