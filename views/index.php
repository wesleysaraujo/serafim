<?php include("_header.php"); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="well">
				<p>Template view Index.php</p>
				<h3>Esse template de aplicavo usa:</h3>
				<ul>
					<li><a href="http://www.slimframework.com" target="_blank">Slim Framework</a></li>
					<li><a href="http://www.flathemes.com/index.html" target="_blank">tema BootFlat 1.0.1 com Bootstrap 3</a></li>
					<li><a href="http://jquery.com" target="_blank">jQuery</a></li>
				</ul>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<a href="javascript:{}" id="btn-exemp-event" class="btn btn-block btn-primary">Clique aqui pra testar a chamada ajax do app.js na rota /hello_json</a>
				</div>
			</div>
			<div id="response">
				
			</div>
		</div>
	</div>
</div>
<?php include("_footer.php"); ?>