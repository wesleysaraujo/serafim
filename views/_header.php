<!DOCTYPE html>
<html>
    <head>
      <title>App Template (Slim Framework) Serafim</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!-- Bootstrap -->
      <link href="<?php echo $base_url ?>/public/assets/bootstrap/bootstrap.min.css" rel="stylesheet" media="screen">
      <link href="<?php echo $base_url ?>/public/assets/css/font-awesome.min.css" rel="stylesheet" media="screen">
      <link href="<?php echo $base_url ?>/public/assets/css/bootflat.css" rel="stylesheet" media="screen">
      <link href="<?php echo $base_url ?>/public/assets/css/bootflat-extensions.css" rel="stylesheet" media="screen">
      <link href="<?php echo $base_url ?>/public/assets/css/bootflat-square.css" rel="stylesheet" media="screen">

      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
        <script src="<?php echo $base_url ?>public/assets/js/html5shiv.js"></script>
        <script src="<?php echo $base_url ?>public/assets/js/respond.min.js"></script>
      <![endif]-->
    </head>
    <body>
    <div class="jumbotron">
      <div class="container">
        <div class="row">
          <h1>Bem vindo ao App template Serafim</h1>
        </div>
      </div>
    </div>
    