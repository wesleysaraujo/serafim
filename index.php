<?php 
//Chama arquivo de configuração e manipulação do banco de dados
require'config/config.php';
require'config/DB.php';

//Chama o Slim
require 'vendors/Slim/Slim.php';
//Constantes para o caminho dos controllers e Views
define('CONTROLLERS_PATH', './controllers/');
define('VIEWS_PATH', './views');

\Slim\Slim::registerAutoloader();
//Instancia o Slim
$app = new \Slim\Slim(array(
    'templates.path' => VIEWS_PATH
));

//Global base_url (Altere o valor da chave base_url para o seu domínio)
$app->hook('slim.before', function () use ($app) {
    $app->view()->appendData(array('base_url' => __BASE_URL__));
});

$app->error(function ( Exception $e = null) use ($app) {
    echo '{"error":{"text":"'. $e->getMessage() .'"}}';
});

//Lê todos os controllers do diretório controllers/ e procura qual tem a rota da requisição
$controller_dir = opendir(CONTROLLERS_PATH);
while ($controller = readdir($controller_dir))
{
    if($controller != '.' and $controller != '..')
        require CONTROLLERS_PATH . $controller;
}
//Função que formata o resultado no formato json
function format_json($obj)
{
    echo json_encode($obj);
}
//Roda o slim framework
$app->run();

?>