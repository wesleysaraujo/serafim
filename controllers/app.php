<?php

$app->get('/', function() use ($app) {
	$app->render('index.php');
});

$app->get('/hello_json', function() use ($app){
	$hello = "Hello World";
	format_json(array('status' => 200, 'message' => $hello));
});