<?php

/*
* Classe para conexāo com o banco de dados Mysql,
* Via acesso nativo do PHP/PDO
* É necessario ter definido as seguintes constantes: DB_NAME, DB_HOST, DB_USER, DB_PASSWORD no arquivo config.php
* @author: Wesley S. Araújo
*/
class DB
{
	/****
	* Instãncia singleton
	* @var DB
	*/

	private static $instance;

	/**
	* Conexāo com o banco de dadis
	* @var PDO
	*/

	private static $connection;

	/**
	* Construtor privado da classe singleton
	*/
	private function __construct()
	{
		self::$connection = new PDO("mysql:dbname=".DB_NAME . ";host=" . DB_HOST, DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		self::$connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
	}

	/**
	* Obtém a instancia da classe DB
	* @return type
	*/
	public static function getInstance()
	{
		if(empty(self::$instance))
		{
			self::$instance = new DB();
		}

		return self::$instance;
	}

	/**
	* Return a conexāo PDO com o banco de dados
	* @return PDO
	*/
	public static function getConn()
	{
		self::getInstance();
		return self::$connection;
	}

	/**
	* Prepapra a SQL para ser executada posteriormente
	* @param String $sql
	* @return PDOStatement stmt
	*/
	public static function prepare($sql)
	{
		return self::getConn()->prepare($sql);
	}

	/**
	* Retorna o id da última consulta INSERT
	* @return int
	*/
	public static function lastInsertId()
	{
		return self::getConn()->lasInsertId();
	}

	/**
	* Inicia uma transaçāo
	* @return bool
	*/
	public static function beginTransaction()
	{
		return self::getConn()->beginTransaction();
	}

	/**
	* Commita uma transaçāo
	* @return bool
	*/
	public static function commit()
	{
		return self::getConn()->commit();
	}

	/**
	* Realiza um rollback na transaçāo
	* @return bool
	*/
	public static function rollBack()
	{
		return self::getConn()->rollBack();
	}

	/**
	* Formata uma data para o Mysql (05/12/2014 para 2014-12-05)
	* @param type $date
	* @return type
	*/
	public static function dateToMysql($date)
	{
		return implode("-", array_reverse(explode("/", $date)));
	}

	/**
	* Formata uma data do Mysql (2014-01-21 para 21/01/2014)
	* @param type $date
	* @return type
	*/
	public static function dateFromMysql($date)
	{
		return implode("/", array_reverse(explode("-", $date)));
	}
}